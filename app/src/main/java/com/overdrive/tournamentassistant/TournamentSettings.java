package com.overdrive.tournamentassistant;

import java.io.Serializable;

/**
 * Created by Overdrive on 13.07.2018.
 */

public class TournamentSettings implements Serializable {

    private NumberOfPlayers mNumberOfPlayers;
    private TypeOfTournament mTypeOfTournament;
    private TypeOfGroupMatchesSixPlayers mTypeOfGroupMatchesSixPlayers;
    private ScoreSixPlayers mScoreSixPlayers;

    private boolean mShuffle;



    public static final int SINGLE_4_GAMES_FIRST_TOUR = 2;
    public static final int SINGLE_4_GAMES_SECOND_TOUR = 1;
    public static final int SINGLE_4_WINNERS_FIRST_TOUR = 2;
    public static final int SINGLE_4_LOSERS_FIRST_TOUR = 2;
    public static final int SINGLE_4_WINNERS_SECOND_TOUR = 1;
    public static final int SINGLE_4_LOSERS_SECOND_TOUR = 1;

    public static final int DOUBLE_4_GAMES_FIRST_TOUR = 2;
    public static final int DOUBLE_4_GAMES_SECOND_TOUR = 1;
    public static final int DOUBLE_4_GAMES_THIRD_TOUR = 1;
    public static final int DOUBLE_4_GAMES_FOURTH_TOUR = 1;
    public static final int DOUBLE_4_GAMES_FIFTH_TOUR = 1;
    public static final int DOUBLE_4_WINNERS_FIRST_TOUR = 2;
    public static final int DOUBLE_4_LOSERS_FIRST_TOUR = 2;
    public static final int DOUBLE_4_WINNERS_SECOND_TOUR = 1;
    public static final int DOUBLE_4_LOSERS_SECOND_TOUR = 1;
    public static final int DOUBLE_4_WINNERS_THIRD_TOUR = 1;
    public static final int DOUBLE_4_LOSERS_THIRD_TOUR = 1;
    public static final int DOUBLE_4_WINNERS_FOURTH_TOUR = 1;
    public static final int DOUBLE_4_LOSERS_FOURTH_TOUR = 1;
    public static final int DOUBLE_4_WINNERS_FIFTH_TOUR = 1;
    public static final int DOUBLE_4_LOSERS_FIFTH_TOUR = 1;

    public static final int GROUP_6_SINGLE_4_GAMES_PER_TOUR = 2;
    public static final int GROUP_6_SINGLE_4_WINNERS = 4;
    public static final int GROUP_6_SINGLE_4_LOSERS = 2;

    public static final int GROUP_6_DOUBLE_4_GAMES_PER_TOUR = 2;
    public static final int GROUP_6_DOUBLE_4_WINNERS = 4;
    public static final int GROUP_6_DOUBLE_4_LOSERS = 2;

    public static final int GROUP_6_DOUBLE_6_GAMES_PER_TOUR = 2;
    public static final int GROUP_6_DOUBLE_6_WINNERS = 6;
    public static final int GROUP_6_DOUBLE_6_LOSERS = 0;

    public static final int DOUBLE_6_GAMES_FIRST_TOUR = 2;
    public static final int DOUBLE_6_GAMES_SECOND_TOUR = 1;
    public static final int DOUBLE_6_GAMES_THIRD_TOUR = 1;
    public static final int DOUBLE_6_GAMES_FOURTH_TOUR = 1;
    public static final int DOUBLE_6_GAMES_FIFTH_TOUR = 1;
    public static final int DOUBLE_6_WINNERS_FIRST_TOUR = 2;
    public static final int DOUBLE_6_LOSERS_FIRST_TOUR = 2;
    public static final int DOUBLE_6_WINNERS_SECOND_TOUR = 1;
    public static final int DOUBLE_6_LOSERS_SECOND_TOUR = 1;
    public static final int DOUBLE_6_WINNERS_THIRD_TOUR = 1;
    public static final int DOUBLE_6_LOSERS_THIRD_TOUR = 1;
    public static final int DOUBLE_6_WINNERS_FOURTH_TOUR = 1;
    public static final int DOUBLE_6_LOSERS_FOURTH_TOUR = 1;
    public static final int DOUBLE_6_WINNERS_FIFTH_TOUR = 1;
    public static final int DOUBLE_6_LOSERS_FIFTH_TOUR = 1;

    public static final int SINGLE_8_GAMES_FIRST_TOUR = 4;
    public static final int SINGLE_8_GAMES_SECOND_TOUR = 2;
    public static final int SINGLE_8_GAMES_THIRD_TOUR = 1;
    public static final int SINGLE_8_WINNERS_FIRST_TOUR = 4;
    public static final int SINGLE_8_LOSERS_FIRST_TOUR = 4;
    public static final int SINGLE_8_WINNERS_SECOND_TOUR = 2;
    public static final int SINGLE_8_LOSERS_SECOND_TOUR = 2;
    public static final int SINGLE_8_WINNERS_THIRD_TOUR = 1;
    public static final int SINGLE_8_LOSERS_THIRD_TOUR = 1;

    public static final int DOUBLE_8_GAMES_FIRST_TOUR = 4;
    public static final int DOUBLE_8_GAMES_SECOND_TOUR = 2;
    public static final int DOUBLE_8_GAMES_THIRD_TOUR = 2;
    public static final int DOUBLE_8_GAMES_FOURTH_TOUR = 2;
    public static final int DOUBLE_8_GAMES_FIFTH_TOUR = 1;
    public static final int DOUBLE_8_GAMES_SIXTH_TOUR = 1;
    public static final int DOUBLE_8_GAMES_SEVENTH_TOUR = 1;
    public static final int DOUBLE_8_GAMES_EIGHTH_TOUR = 1;
    public static final int DOUBLE_8_WINNERS_FIRST_TOUR = 4;
    public static final int DOUBLE_8_LOSERS_FIRST_TOUR = 4;
    public static final int DOUBLE_8_WINNERS_SECOND_TOUR = 2;
    public static final int DOUBLE_8_LOSERS_SECOND_TOUR = 2;
    public static final int DOUBLE_8_WINNERS_THIRD_TOUR = 2;
    public static final int DOUBLE_8_LOSERS_THIRD_TOUR = 2;
    public static final int DOUBLE_8_WINNERS_FOURTH_TOUR = 2;
    public static final int DOUBLE_8_LOSERS_FOURTH_TOUR = 2;
    public static final int DOUBLE_8_WINNERS_FIFTH_TOUR = 1;
    public static final int DOUBLE_8_LOSERS_FIFTH_TOUR = 1;
    public static final int DOUBLE_8_WINNERS_SIXTH_TOUR = 1;
    public static final int DOUBLE_8_LOSERS_SIXTH_TOUR = 1;
    public static final int DOUBLE_8_WINNERS_SEVENTH_TOUR = 1;
    public static final int DOUBLE_8_LOSERS_SEVENTH_TOUR = 1;
    public static final int DOUBLE_8_WINNERS_EIGHTH_TOUR = 1;
    public static final int DOUBLE_8_LOSERS_EIGHTH_TOUR = 1;



    public TournamentSettings(NumberOfPlayers numberOfPlayers, TypeOfTournament typeOfTournament) {
        mNumberOfPlayers = numberOfPlayers;
        mTypeOfTournament = typeOfTournament;
        mShuffle = true;
    }

    public TournamentSettings(NumberOfPlayers numberOfPlayers, TypeOfTournament typeOfTournament,
                              TypeOfGroupMatchesSixPlayers typeOfGroupMatchesSixPlayers,
                              ScoreSixPlayers scoreSixPlayers) {

        this(numberOfPlayers, typeOfTournament);
        mTypeOfGroupMatchesSixPlayers = typeOfGroupMatchesSixPlayers;
        mScoreSixPlayers = scoreSixPlayers;
    }

    public NumberOfPlayers getNumberOfPlayers() {
        return mNumberOfPlayers;
    }

    public int getNumberOfPlayersInt() {
        switch(mNumberOfPlayers) {
            case FOUR:
                return 4;
            case SIX:
                return 6;
            case EIGHT:
                return 8;
        }
        return 4;
    }

    public void setNumberOfPlayers(NumberOfPlayers numberOfPlayers) {
        mNumberOfPlayers = numberOfPlayers;
    }

    public TypeOfTournament getTypeOfTournament() {
        return mTypeOfTournament;
    }

    public void setTypeOfTournament(TypeOfTournament typeOfTournament) {
        mTypeOfTournament = typeOfTournament;
    }

    public TypeOfGroupMatchesSixPlayers getTypeOfGroupMatchesSixPlayers() {
        return mTypeOfGroupMatchesSixPlayers;
    }

    public void setTypeOfGroupMatchesSixPlayers(TypeOfGroupMatchesSixPlayers typeOfGroupMatchesSixPlayers) {
        mTypeOfGroupMatchesSixPlayers = typeOfGroupMatchesSixPlayers;
    }

    public ScoreSixPlayers getScoreSixPlayers() {
        return mScoreSixPlayers;
    }

    public void setScoreSixPlayers(ScoreSixPlayers scoreSixPlayers) {
        mScoreSixPlayers = scoreSixPlayers;
    }

    public boolean isShuffle() {
        return mShuffle;
    }

    public void setShuffle(boolean shuffle) {
        mShuffle = shuffle;
    }



    public enum NumberOfPlayers {
        FOUR, SIX, EIGHT
    }

    public enum TypeOfTournament {
        SINGLE_4, DOUBLE_4, GROUP_6_SINGLE_4, GROUP_6_DOUBLE_4, GROUP_6_DOUBLE_6, DOUBLE_6, SINGLE_8, DOUBLE_8
    }

    public enum TypeOfGroupMatchesSixPlayers {
        BO1, BO2, BO3
    }

    public enum ScoreSixPlayers {
        GAME, BALL
    }

    public int getGamesConstant(int tourOfAGame) {
        switch(getNumberOfPlayers()){
            case FOUR:
                switch(getTypeOfTournament()) {
                    case SINGLE_4:
                        switch(tourOfAGame) {
                            case 1:
                                return SINGLE_4_GAMES_FIRST_TOUR;
                            case 2:
                                return SINGLE_4_GAMES_SECOND_TOUR;
                        }
                        break;
                    case DOUBLE_4:
                        switch(tourOfAGame) {
                            case 1:
                                return DOUBLE_4_GAMES_FIRST_TOUR;
                            case 2:
                                return DOUBLE_4_GAMES_SECOND_TOUR;
                            case 3:
                                return DOUBLE_4_GAMES_THIRD_TOUR;
                            case 4:
                                return DOUBLE_4_GAMES_FOURTH_TOUR;
                            case 5:
                                return DOUBLE_4_GAMES_FIFTH_TOUR;
                        }
                        break;
                }
                break;

            case SIX:
                switch(getTypeOfTournament()) {
                    case GROUP_6_SINGLE_4:
                        return GROUP_6_SINGLE_4_GAMES_PER_TOUR;
                    case GROUP_6_DOUBLE_4:
                        return GROUP_6_DOUBLE_4_GAMES_PER_TOUR;
                    case GROUP_6_DOUBLE_6:
                        return GROUP_6_DOUBLE_6_GAMES_PER_TOUR;

                    case DOUBLE_6:
                        switch(tourOfAGame) {
                            case 1:
                                return DOUBLE_6_GAMES_FIRST_TOUR;
                            case 2:
                                return DOUBLE_6_GAMES_SECOND_TOUR;
                            case 3:
                                return DOUBLE_6_GAMES_THIRD_TOUR;
                            case 4:
                                return DOUBLE_6_GAMES_FOURTH_TOUR;
                            case 5:
                                return DOUBLE_6_GAMES_FIFTH_TOUR;
                        }
                        break;
                }
                break;

            case EIGHT:
                switch(getTypeOfTournament()) {
                    case SINGLE_8:
                        switch(tourOfAGame) {
                            case 1:
                                return SINGLE_8_GAMES_FIRST_TOUR;
                            case 2:
                                return SINGLE_8_GAMES_SECOND_TOUR;
                            case 3:
                                return SINGLE_8_GAMES_THIRD_TOUR;
                        }
                        break;
                    case DOUBLE_8:
                        switch(tourOfAGame) {
                            case 1:
                                return DOUBLE_8_GAMES_FIRST_TOUR;
                            case 2:
                                return DOUBLE_8_GAMES_SECOND_TOUR;
                            case 3:
                                return DOUBLE_8_GAMES_THIRD_TOUR;
                            case 4:
                                return DOUBLE_8_GAMES_FOURTH_TOUR;
                            case 5:
                                return DOUBLE_8_GAMES_FIFTH_TOUR;
                            case 6:
                                return DOUBLE_8_GAMES_SIXTH_TOUR;
                            case 7:
                                return DOUBLE_8_GAMES_SEVENTH_TOUR;
                            case 8:
                                return DOUBLE_8_GAMES_EIGHTH_TOUR;
                        }
                        break;
                }
                break;
        }

        return 0;
    }

    public int getWinnersConstant(int tourOfAGame) {
        switch(getNumberOfPlayers()){
            case FOUR:
                switch(getTypeOfTournament()) {
                    case SINGLE_4:
                        switch(tourOfAGame) {
                            case 1:
                                return SINGLE_4_WINNERS_FIRST_TOUR;
                            case 2:
                                return SINGLE_4_WINNERS_SECOND_TOUR;
                        }
                        break;
                    case DOUBLE_4:
                        switch(tourOfAGame) {
                            case 1:
                                return DOUBLE_4_WINNERS_FIRST_TOUR;
                            case 2:
                                return DOUBLE_4_WINNERS_SECOND_TOUR;
                            case 3:
                                return DOUBLE_4_WINNERS_THIRD_TOUR;
                            case 4:
                                return DOUBLE_4_WINNERS_FOURTH_TOUR;
                            case 5:
                                return DOUBLE_4_WINNERS_FIFTH_TOUR;
                        }
                        break;
                }
                break;

            case SIX:
                switch(getTypeOfTournament()) {
                    case GROUP_6_SINGLE_4:
                        return GROUP_6_SINGLE_4_WINNERS;
                    case GROUP_6_DOUBLE_4:
                        return GROUP_6_DOUBLE_4_WINNERS;
                    case GROUP_6_DOUBLE_6:
                        return GROUP_6_DOUBLE_6_WINNERS;

                    case DOUBLE_6:
                        switch(tourOfAGame) {
                            case 1:
                                return DOUBLE_6_WINNERS_FIRST_TOUR;
                            case 2:
                                return DOUBLE_6_WINNERS_SECOND_TOUR;
                            case 3:
                                return DOUBLE_6_WINNERS_THIRD_TOUR;
                            case 4:
                                return DOUBLE_6_WINNERS_FOURTH_TOUR;
                            case 5:
                                return DOUBLE_6_WINNERS_FIFTH_TOUR;
                        }
                        break;
                }
                break;

            case EIGHT:
                switch(getTypeOfTournament()) {
                    case SINGLE_8:
                        switch(tourOfAGame) {
                            case 1:
                                return SINGLE_8_WINNERS_FIRST_TOUR;
                            case 2:
                                return SINGLE_8_WINNERS_SECOND_TOUR;
                            case 3:
                                return SINGLE_8_WINNERS_THIRD_TOUR;
                        }
                        break;
                    case DOUBLE_8:
                        switch(tourOfAGame) {
                            case 1:
                                return DOUBLE_8_WINNERS_FIRST_TOUR;
                            case 2:
                                return DOUBLE_8_WINNERS_SECOND_TOUR;
                            case 3:
                                return DOUBLE_8_WINNERS_THIRD_TOUR;
                            case 4:
                                return DOUBLE_8_WINNERS_FOURTH_TOUR;
                            case 5:
                                return DOUBLE_8_WINNERS_FIFTH_TOUR;
                            case 6:
                                return DOUBLE_8_WINNERS_SIXTH_TOUR;
                            case 7:
                                return DOUBLE_8_WINNERS_SEVENTH_TOUR;
                            case 8:
                                return DOUBLE_8_WINNERS_EIGHTH_TOUR;
                        }
                        break;
                }
                break;
        }

        return 0;
    }


    public int getLosersConstant(int tourOfAGame) {
        switch(getNumberOfPlayers()){
            case FOUR:
                switch(getTypeOfTournament()) {
                    case SINGLE_4:
                        switch(tourOfAGame) {
                            case 1:
                                return SINGLE_4_LOSERS_FIRST_TOUR;
                            case 2:
                                return SINGLE_4_LOSERS_SECOND_TOUR;
                        }
                        break;
                    case DOUBLE_4:
                        switch(tourOfAGame) {
                            case 1:
                                return DOUBLE_4_LOSERS_FIRST_TOUR;
                            case 2:
                                return DOUBLE_4_LOSERS_SECOND_TOUR;
                            case 3:
                                return DOUBLE_4_LOSERS_THIRD_TOUR;
                            case 4:
                                return DOUBLE_4_LOSERS_FOURTH_TOUR;
                            case 5:
                                return DOUBLE_4_LOSERS_FIFTH_TOUR;
                        }
                        break;
                }
                break;

            case SIX:
                switch(getTypeOfTournament()) {
                    case GROUP_6_SINGLE_4:
                        return GROUP_6_SINGLE_4_LOSERS;
                    case GROUP_6_DOUBLE_4:
                        return GROUP_6_DOUBLE_4_LOSERS;
                    case GROUP_6_DOUBLE_6:
                        return GROUP_6_DOUBLE_6_LOSERS;

                    case DOUBLE_6:
                        switch(tourOfAGame) {
                            case 1:
                                return DOUBLE_6_LOSERS_FIRST_TOUR;
                            case 2:
                                return DOUBLE_6_LOSERS_SECOND_TOUR;
                            case 3:
                                return DOUBLE_6_LOSERS_THIRD_TOUR;
                            case 4:
                                return DOUBLE_6_LOSERS_FOURTH_TOUR;
                            case 5:
                                return DOUBLE_6_LOSERS_FIFTH_TOUR;
                        }
                        break;
                }
                break;

            case EIGHT:
                switch(getTypeOfTournament()) {
                    case SINGLE_8:
                        switch(tourOfAGame) {
                            case 1:
                                return SINGLE_8_LOSERS_FIRST_TOUR;
                            case 2:
                                return SINGLE_8_LOSERS_SECOND_TOUR;
                            case 3:
                                return SINGLE_8_LOSERS_THIRD_TOUR;
                        }
                        break;
                    case DOUBLE_8:
                        switch(tourOfAGame) {
                            case 1:
                                return DOUBLE_8_LOSERS_FIRST_TOUR;
                            case 2:
                                return DOUBLE_8_LOSERS_SECOND_TOUR;
                            case 3:
                                return DOUBLE_8_LOSERS_THIRD_TOUR;
                            case 4:
                                return DOUBLE_8_LOSERS_FOURTH_TOUR;
                            case 5:
                                return DOUBLE_8_LOSERS_FIFTH_TOUR;
                            case 6:
                                return DOUBLE_8_LOSERS_SIXTH_TOUR;
                            case 7:
                                return DOUBLE_8_LOSERS_SEVENTH_TOUR;
                            case 8:
                                return DOUBLE_8_LOSERS_EIGHTH_TOUR;
                        }
                        break;
                }
                break;
        }

        return 0;
    }
}
