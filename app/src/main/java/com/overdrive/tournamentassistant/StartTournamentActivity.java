package com.overdrive.tournamentassistant;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class StartTournamentActivity extends SingleFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
    }

    @Override
    protected Fragment createFragment() {
        return new StartTournamentFragment();
    }
}
