package com.overdrive.tournamentassistant;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;

import com.overdrive.tournamentassistant.TournamentSettings.NumberOfPlayers;
import com.overdrive.tournamentassistant.TournamentSettings.TypeOfTournament;

/**
 * Created by Overdrive on 13.07.2018.
 */

public class SettingTournamentFragment extends Fragment {

    private static final String TAG = "SettingTournament";
    private static final int REQUEST_CODE_MAIN_TOURNAMENT = 0;

    private Spinner mNumberSpinner;
    private Spinner mTypeSpinner;
    private Button mContinueButton;
    private CheckBox mShuffleCheckBox;

    private TournamentSettings mTournamentSettings;

    private static final String[] NUMBERS_OF_PLAYERS = {"4", "6", "8"};

    private static final String[] TYPES_OF_TOURNAMENTS_FOUR_PLAYERS = {
            "Single elimination",
            "Double elimination"
    };
    private static final String[] TYPES_OF_TOURNAMENTS_SIX_PLAYERS = {
            "Group/Single 4 players",
            "Group/Double 4 players",
            "Group/Double 6 players"
    };
    private static final String[] TYPES_OF_TOURNAMENTS_EIGHT_PLAYERS = {
            "Single elimination",
            "Double elimination"
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setting_tournament, container, false);

        Log.d(TAG, "onCreateView");
        mTournamentSettings = new TournamentSettings(NumberOfPlayers.FOUR,
                TypeOfTournament.SINGLE_4);

        ArrayAdapter<String> numberAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.spinner_item, NUMBERS_OF_PLAYERS);
        numberAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

        mNumberSpinner = (Spinner)v.findViewById(R.id.spinner_players);
        mNumberSpinner.setAdapter(numberAdapter);


        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.spinner_item, TYPES_OF_TOURNAMENTS_FOUR_PLAYERS);
        typeAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

        mTypeSpinner = (Spinner)v.findViewById(R.id.spinner_type);
        mTypeSpinner.setAdapter(typeAdapter);


        mNumberSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View itemSelected,
                                       int selectedItemPosition, long selectedId) {
                String[] spinner_types = null;
                switch(selectedItemPosition) {
                    case 0:
                        spinner_types = TYPES_OF_TOURNAMENTS_FOUR_PLAYERS;
                        mTournamentSettings.setNumberOfPlayers(
                                NumberOfPlayers.FOUR);
                        break;
                    case 1:
                        spinner_types = TYPES_OF_TOURNAMENTS_SIX_PLAYERS;
                        mTournamentSettings.setNumberOfPlayers(
                                NumberOfPlayers.SIX);
                        break;
                    case 2:
                        spinner_types = TYPES_OF_TOURNAMENTS_EIGHT_PLAYERS;
                        mTournamentSettings.setNumberOfPlayers(
                                NumberOfPlayers.EIGHT);
                        break;
                    default:
                        spinner_types = TYPES_OF_TOURNAMENTS_FOUR_PLAYERS;
                        mTournamentSettings.setNumberOfPlayers(
                                NumberOfPlayers.FOUR);
                }
                ArrayAdapter<String> typeAdapterTemp = new ArrayAdapter<>(getActivity(),
                        R.layout.spinner_item, spinner_types);
                typeAdapterTemp .setDropDownViewResource(R.layout.spinner_dropdown_item);

                mTypeSpinner.setAdapter(typeAdapterTemp);


                Log.d(TAG, mTournamentSettings.getNumberOfPlayers().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                Log.d(TAG, "Nothing");
            }
        });


        mTypeSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View itemSelected,
                                       int selectedItemPosition, long selectedId) {

                if(mTournamentSettings.getNumberOfPlayers() ==
                        NumberOfPlayers.FOUR) {
                    switch (selectedItemPosition) {
                        case 0:
                            mTournamentSettings.setTypeOfTournament(TypeOfTournament.SINGLE_4);
                            break;
                        case 1:
                            mTournamentSettings.setTypeOfTournament(TypeOfTournament.DOUBLE_4);
                            break;
                    }
                }

                if(mTournamentSettings.getNumberOfPlayers() ==
                        NumberOfPlayers.SIX) {
                    switch (selectedItemPosition) {
                        case 0:
                            mTournamentSettings.setTypeOfTournament(TypeOfTournament.GROUP_6_SINGLE_4);
                            break;
                        case 1:
                            mTournamentSettings.setTypeOfTournament(TypeOfTournament.GROUP_6_DOUBLE_4);
                            break;
                        case 2:
                            mTournamentSettings.setTypeOfTournament(TypeOfTournament.GROUP_6_DOUBLE_6);
                            break;
                    }
                }
                if(mTournamentSettings.getNumberOfPlayers() ==
                        NumberOfPlayers.EIGHT) {
                    switch (selectedItemPosition) {
                        case 0:
                            mTournamentSettings.setTypeOfTournament(TypeOfTournament.SINGLE_8);
                            break;
                        case 1:
                            mTournamentSettings.setTypeOfTournament(TypeOfTournament.DOUBLE_8);
                            break;
                    }
                }

                Log.d(TAG, mTournamentSettings.getTypeOfTournament().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }

        });


        mShuffleCheckBox = (CheckBox)v.findViewById(R.id.check_box_shuffle);
        mShuffleCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mTournamentSettings.setShuffle(!mShuffleCheckBox.isChecked());
            }
        });

        mContinueButton = (Button)v.findViewById(R.id.button_settings_continue);
        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(MainTournamentActivity.getStartIntent(getActivity(),
                        mTournamentSettings), REQUEST_CODE_MAIN_TOURNAMENT);
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if(requestCode == REQUEST_CODE_MAIN_TOURNAMENT) {
            getActivity().finish();
        }

    }

}
