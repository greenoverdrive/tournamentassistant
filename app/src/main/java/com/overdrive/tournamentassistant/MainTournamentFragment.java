package com.overdrive.tournamentassistant;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by Overdrive on 13.07.2018.
 */

public class MainTournamentFragment extends Fragment {

    private static final String TAG = "MainTournament";
    private static final String BUNDLE_SETTINGS = "tournament_settings";
    private static final int REQUEST_CODE_WINNER = 0;

    private static final String SAVED_PLAYERS = "players";
    private static final String SAVED_WINNERS = "winners";
    private static final String SAVED_LOSERS = "losers";
    private static final String SAVED_WINNERS_UPPER = "winners_upper";
    private static final String SAVED_WINNERS_LOWER = "winners_lower";
    private static final String SAVED_LOSERS_SIX_HELPER = "losers_six_helper";
    private static final String SAVED_GROUP_SCORE_FIRST_GAME = "group_score_first_game";
    private static final String SAVED_GROUP_SCORE_SECOND_GAME = "group_score_second_game";
    private static final String SAVED_SINGLE_GAME_PLAYED = "single_game_played";
    private static final String SAVED_PLAYERS_TABLE = "players_table";

    private static final String SAVED_TOUR = "tour_of_a_game";
    private static final String SAVED_NUMBER_OF_REMATCH = "number_of_rematch";
    private static final String SAVED_NAMES_ENTERED = "is_names_entered";
    private static final String SAVED_GAMES_PLAYED = "is_games_played";
    private static final String SAVED_GROUP_PLAYED = "is_group_stage_played";
    private static final String SAVED_GROUP_ENDING = "is_group_ending_set";
    private static final String SAVED_REMATCH_ANALYZE = "is_rematch_analyze";
    private static final String SAVED_TRIPLE_REMATCH_FIRST_GROUP = "is_triple_rematch_first_group";
    private static final String SAVED_TRIPLE_REMATCH_SECOND_GROUP = "is_triple_rematch_second_group";

    private RecyclerView mTournamentRecyclerView;
    private Button mContinueButton;
    private TextView mTitleTextView;

    private TournamentSettings mTournamentSettings;
    private ArrayList<String> mPlayers;

    private ArrayList<String> mPlayersTable;

    private ArrayList<String> mWinnersOfATour;
    private ArrayList<String> mLosersOfATour;
    private ArrayList<String> mWinnersOfATourUpperBracket;
    private ArrayList<String> mWinnersOfATourLowerBracket;
    private ArrayList<String> mLosersOfATourUpperBracketDoubleSixHelper;
    private int[] mPlayersGroupScoreFirstGame;
    private int[] mPlayersGroupScoreSecondGame;

    private boolean[] mIsASingleGamePlayed;

    private int mTourOfAGame = 0;
    private int mNumberOfRematchGames = 0;
    private boolean mIsNamesEntered = false;
    private boolean mIsGamesPlayed = true;
    private boolean mIsGroupStagePlayed = false;
    private boolean mIsGroupEndingSet = false;
    private boolean mIsRematchAnalyze = false;
    private boolean mIsTripleRematchFirstGroup = false;
    private boolean mIsTripleRematchSecondGroup = false;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setHasOptionsMenu(true);
        loadSavedState(savedInstanceState);

        mTournamentSettings = (TournamentSettings)getArguments().getSerializable(BUNDLE_SETTINGS);

        mPlayers = new ArrayList<>(mTournamentSettings.getNumberOfPlayersInt());
        for(int i = 0; i < mTournamentSettings.getNumberOfPlayersInt(); i++) {
            mPlayers.add("");
        }

        clearTourArrays();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main_tournament, container, false);

        mTitleTextView = (TextView)v.findViewById(R.id.text_title_main_tournament);

        mTournamentRecyclerView = (RecyclerView)v.findViewById(R.id.recycler_main_tournament);
        mTournamentRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mTournamentRecyclerView.setAdapter(
                new EnterNamesAdapter(mTournamentSettings.getNumberOfPlayersInt()));

        mContinueButton = (Button)v.findViewById(R.id.button_continue_main_tournament);
        mContinueButton.setEnabled(false);
        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mIsNamesEntered && mIsGamesPlayed) {
                    mContinueButton.setEnabled(false);
                    mTourOfAGame++;

                    hideKeyboard(getActivity());
                    tournamentPlay();
                }
            }
        });

        return v;
    }

    private void tournamentPlay() {
        mIsGamesPlayed = false;

        switch(mTournamentSettings.getNumberOfPlayers()) {
            case FOUR:

                switch(mTournamentSettings.getTypeOfTournament()) {
                    case SINGLE_4:
                        single4Play();
                        break;

                    case DOUBLE_4:
                        double4Play();
                        break;
                }

                break;
            case SIX:
                switch(mTournamentSettings.getTypeOfTournament()) {
                    case GROUP_6_SINGLE_4:
                    case GROUP_6_DOUBLE_4:
                        groupPlay();
                        break;

                    case GROUP_6_DOUBLE_6:
                        group6Play();
                        break;

                    case DOUBLE_6:
                        double6Play();
                        break;
                }

                break;

            case EIGHT:
                switch(mTournamentSettings.getTypeOfTournament()) {
                    case SINGLE_8:
                        single8Play();
                        break;

                    case DOUBLE_8:
                        double8Play();
                        break;
                }
                break;
        }
    }


    private class EnterNameHolder extends RecyclerView.ViewHolder {

        private TextView mNumberTextView;
        private TextView mNameTextView;
        private int mPosition;

        public EnterNameHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.enter_names_item, parent, false));

            mNumberTextView = (TextView)itemView.findViewById(R.id.enter_name_item_number);
            mNameTextView = (TextView)itemView.findViewById(R.id.enter_name_item_name);

            mNameTextView.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mPlayers.set(mPosition, s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                    for(String str: mPlayers) {
                        if(str.equals("")) {
                            mContinueButton.setEnabled(false);
                            return;
                        }
                    }
                    mIsNamesEntered = true;
                    mContinueButton.setEnabled(true);
                }
            });
        }

        public void bind(int position) {
            mPosition = position;

            mNumberTextView.setText(getResources()
                    .getString(R.string.item_number, mPosition + 1));
            mNameTextView.setText(mPlayers.get(mPosition));
        }

    }

    private class EnterNamesAdapter extends RecyclerView.Adapter<EnterNameHolder> {

        private int mNumberOfPlayers;

        public EnterNamesAdapter(int numberOfPlayers) {
            mNumberOfPlayers = numberOfPlayers;
        }

        @Override
        @NonNull
        public EnterNameHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new EnterNameHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull EnterNameHolder holder, int position) {
            holder.bind(position);
        }

        @Override
        public int getItemCount() {
            return mNumberOfPlayers;
        }

    }


    private class TournamentGamesHolder extends RecyclerView.ViewHolder {

        private TextView mLeftPlayerTextView;
        private TextView mRightPlayerTextView;
        private TextView mNumberTextView;
        private int mPosition;

        public TournamentGamesHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.tournament_one_game_item, parent, false));

            mLeftPlayerTextView = (TextView)itemView.findViewById(R.id.main_tournament_item_left_player);
            mRightPlayerTextView = (TextView)itemView.findViewById(R.id.main_tournament_item_right_player);
            mNumberTextView = (TextView)itemView.findViewById(R.id.main_tournament_item_number);

            mLeftPlayerTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLeftPlayerTextView.setBackground(
                            getResources().getDrawable(R.drawable.selected_player_background));
                    mRightPlayerTextView.setBackground(null);

                    mWinnersOfATour.set(mPosition, mLeftPlayerTextView.getText().toString());
                    if(mLosersOfATour.size() != 0) {
                        mLosersOfATour.set(mPosition, mRightPlayerTextView.getText().toString());
                    }

                    mIsASingleGamePlayed[mPosition] = true;

                    checkIsAllGamesPlayed();
                }
            });

            mRightPlayerTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mRightPlayerTextView.setBackground(
                            getResources().getDrawable(R.drawable.selected_player_background));
                    mLeftPlayerTextView.setBackground(null);

                    mWinnersOfATour.set(mPosition, mRightPlayerTextView.getText().toString());
                    if(mLosersOfATour.size() != 0) {
                        mLosersOfATour.set(mPosition, mLeftPlayerTextView.getText().toString());
                    }

                    mIsASingleGamePlayed[mPosition] = true;

                    checkIsAllGamesPlayed();
                }
            });
        }

        private void checkIsAllGamesPlayed() {
            for(boolean i: mIsASingleGamePlayed) {
                if (!i) {
                    return;
                }
            }

            mIsGamesPlayed = true;
            mContinueButton.setEnabled(true);
        }

        public void bind(int position, ArrayList<String> players) {
            mPosition = position;
            mLeftPlayerTextView.setText(players.get(2*position));
            mRightPlayerTextView.setText(players.get(2*position + 1));
            mNumberTextView.setText(getResources()
                    .getString(R.string.item_number, position+1));
        }

    }

    private class TournamentGamesAdapter extends RecyclerView.Adapter<TournamentGamesHolder> {

        private int mNumberOfGames;
        private ArrayList<String> mStagePlayers;

        public TournamentGamesAdapter(int numberOfGames, ArrayList<String> players) {
            mNumberOfGames = numberOfGames;
            mStagePlayers = players;
        }

        @Override
        @NonNull
        public TournamentGamesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new TournamentGamesHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull TournamentGamesHolder holder, int position) {
            holder.bind(position, mStagePlayers);
        }

        @Override
        public int getItemCount() {
            return mNumberOfGames;
        }

    }


    private class TournamentGamesGroupHolder extends RecyclerView.ViewHolder {

        private TextView mLeftPlayerTextView;
        private TextView mRightPlayerTextView;
        private TextView mNumberTextView;

        private EditText mLeftPlayerScore;
        private EditText mRightPlayerScore;

        private int mPosition;

        public TournamentGamesGroupHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.tournament_one_game_item_score, parent, false));

            mLeftPlayerTextView = (TextView)itemView.findViewById(R.id.main_tournament_item_left_player);
            mRightPlayerTextView = (TextView)itemView.findViewById(R.id.main_tournament_item_right_player);
            mNumberTextView = (TextView)itemView.findViewById(R.id.main_tournament_item_number);

            mLeftPlayerScore = (EditText)itemView.findViewById(R.id.main_tournament_item_left_player_score);
            mRightPlayerScore = (EditText)itemView.findViewById(R.id.main_tournament_item_right_player_score);

            mLeftPlayerTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(mRightPlayerTextView.getBackground() != null &&
                            mLeftPlayerTextView.getBackground() == null) {
                        mRightPlayerScore.setText(mLeftPlayerScore.getText());
                    }


                    mLeftPlayerTextView.setBackground(
                            getResources().getDrawable(R.drawable.selected_player_background));
                    mRightPlayerTextView.setBackground(null);


                    checkMaxScoreError();
                    mLeftPlayerScore.setText(R.string.max_score_ping_pong);

                    checkIsAllGamesPlayed();
                }
            });


            mRightPlayerTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(mRightPlayerTextView.getBackground() == null &&
                            mLeftPlayerTextView.getBackground() != null ) {
                        mLeftPlayerScore.setText(mRightPlayerScore.getText());
                    }


                    mRightPlayerTextView.setBackground(
                            getResources().getDrawable(R.drawable.selected_player_background));
                    mLeftPlayerTextView.setBackground(null);


                    checkMaxScoreError();
                    mRightPlayerScore.setText(R.string.max_score_ping_pong);


                    checkIsAllGamesPlayed();
                }
            });


            mLeftPlayerScore.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String str = s.toString();

                    try {
                        if (Integer.parseInt(str) > 11) {
                            mLeftPlayerScore.setText(R.string.max_score_ping_pong);
                        } else if (Integer.parseInt(str) < 0) {
                            mLeftPlayerScore.setText(R.string.min_score_ping_pong);
                        }
                    } catch(NumberFormatException nfe) { }


                    checkMaxScoreError();
                    setPlayersScore();
                    checkIsAllGamesPlayed();
                }
            });

            mRightPlayerScore.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String str = s.toString();
                    try {
                        if(Integer.parseInt(str) > 11) {
                            mRightPlayerScore.setText(R.string.max_score_ping_pong);
                        } else if (Integer.parseInt(str) < 0) {
                            mRightPlayerScore.setText(R.string.min_score_ping_pong);
                        }
                    } catch(NumberFormatException nfe) { }

                    checkMaxScoreError();
                    setPlayersScore();
                    checkIsAllGamesPlayed();
                }
            });
        }

        public void bind(int position, ArrayList<String> players) {
            mPosition = position;

            Pair<Integer, Integer> index = getLeftAndRightPlayersIndex(mPosition);

            mLeftPlayerTextView.setText(players.get(index.first));
            mRightPlayerTextView.setText(players.get(index.second));
            mNumberTextView.setText(getResources()
                    .getString(R.string.item_number, position+1));
        }


        private void checkIsAllGamesPlayed() {
            if(mLeftPlayerScore.getText().toString().isEmpty() ||
                    mRightPlayerScore.getText().toString().isEmpty() ||
                    mLeftPlayerScore.getText().toString().equals("") ||
                    mRightPlayerScore.getText().toString().equals("")||
                    (mRightPlayerTextView.getBackground() == null &&
                    mLeftPlayerTextView.getBackground() == null) ) {
                mIsASingleGamePlayed[mPosition] = false;
            } else {
                mIsASingleGamePlayed[mPosition] = true;
            }

            for(boolean i: mIsASingleGamePlayed) {
                if (!i) {
                    mIsGamesPlayed = false;
                    mContinueButton.setEnabled(false);
                    return;
                }
            }

            mIsGamesPlayed = true;
            mContinueButton.setEnabled(true);
        }

        private void setPlayersScore() {
            if(!mLeftPlayerScore.getText().toString().isEmpty() &
                    !mRightPlayerScore.getText().toString().isEmpty() &&
                    !mLeftPlayerScore.getText().toString().equals("") &&
                    !mRightPlayerScore.getText().toString().equals("")) {

                Pair<Integer, Integer> index = getLeftAndRightPlayersIndex(mPosition);


                if(isAGameFirst(index.first)) {
                    mPlayersGroupScoreFirstGame[index.first] = Integer.
                            parseInt(mLeftPlayerScore.getText().toString());
                } else {
                    mPlayersGroupScoreSecondGame[index.first] = Integer.
                            parseInt(mLeftPlayerScore.getText().toString());
                }

                if(isAGameFirst(index.second)) {
                    mPlayersGroupScoreFirstGame[index.second] = Integer.
                            parseInt(mRightPlayerScore.getText().toString());
                } else {
                    mPlayersGroupScoreSecondGame[index.second] = Integer.
                            parseInt(mRightPlayerScore.getText().toString());
                }

            }
        }

        private Pair<Integer, Integer> getLeftAndRightPlayersIndex(int position) {
            int leftPlayer = 0;
            int rightPlayer = 0;
            switch(mTourOfAGame) {

                case 1:
                    switch (position) {
                        case 0:
                            leftPlayer = 0;
                            rightPlayer = 1;
                            break;
                        case 1:
                            leftPlayer = 3;
                            rightPlayer = 4;
                            break;
                    }
                    break;

                case 2:
                    switch (position) {
                        case 0:
                            leftPlayer = 0;
                            rightPlayer = 2;
                            break;
                        case 1:
                            leftPlayer = 3;
                            rightPlayer = 5;
                            break;
                    }
                    break;

                case 3:
                    switch (position) {
                        case 0:
                            leftPlayer = 1;
                            rightPlayer = 2;
                            break;
                        case 1:
                            leftPlayer = 4;
                            rightPlayer = 5;
                            break;
                    }
                    break;

            }
            return new Pair<Integer, Integer>(leftPlayer, rightPlayer);
        }

        private boolean isAGameFirst(int position) {
            switch(mTourOfAGame) {

                case 1:
                    return true;

                case 2:
                    switch (position) {
                        case 0:
                            return false;
                        case 1:
                            return false;
                        case 2:
                            return true;
                        case 3:
                            return false;
                        case 4:
                            return false;
                        case 5:
                            return true;
                    }
                    break;

                case 3:
                    return false;

            }
            return false;
        }

        private void checkMaxScoreError() {
            if(mRightPlayerTextView.getBackground() != null &&
                    mLeftPlayerScore.getText().toString().equals("11")) {
                mLeftPlayerScore.setText(getString(R.string.ten_points_ping_pong));
            }

            if(mLeftPlayerTextView.getBackground() != null &&
                    mRightPlayerScore.getText().toString().equals("11")) {
                mRightPlayerScore.setText(getString(R.string.ten_points_ping_pong));
            }
        }

    }

    private class TournamentGamesGroupAdapter extends RecyclerView.Adapter<TournamentGamesGroupHolder> {

        private int mNumberOfGames;
        private ArrayList<String> mStagePlayers;

        public TournamentGamesGroupAdapter(int numberOfGames, ArrayList<String> players) {
            mNumberOfGames = numberOfGames;
            mStagePlayers = players;
        }

        @Override
        @NonNull
        public TournamentGamesGroupHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new TournamentGamesGroupHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull TournamentGamesGroupHolder holder, int position) {
            holder.bind(position, mStagePlayers);
        }

        @Override
        public int getItemCount() {
            return mNumberOfGames;
        }

    }




    private void single4Play() {
        switch (mTourOfAGame) {
            case 1:
                mTitleTextView.setText(R.string.title_semifinal);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );

                if(mTournamentSettings.isShuffle()) {
                    Collections.shuffle(mPlayers);
                }


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mPlayers));
                break;
            case 2:
                mTitleTextView.setText(R.string.title_final);

                mPlayersTable.addAll(mLosersOfATour);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        0,
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );

                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATour));

                break;

            case 3:
                showWinner();
                break;
        }
    }

    private void double4Play() {
        switch (mTourOfAGame) {
            case 1:
                mTitleTextView.setText(R.string.title_semifinal_upper);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );

                if(mTournamentSettings.isShuffle()) {
                    Collections.shuffle(mPlayers);
                }

                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mPlayers));

                break;
            case 2:
                mTitleTextView.setText(R.string.title_semifinal_lower);

                mWinnersOfATourUpperBracket = (ArrayList<String>)mWinnersOfATour.clone();

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        0
                );

                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mLosersOfATour));

                break;

            case 3:
                mTitleTextView.setText(R.string.title_final_upper);

                mWinnersOfATourLowerBracket = (ArrayList<String>)mWinnersOfATour.clone();
                mPlayersTable.add(mLosersOfATour.get(0));

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATourUpperBracket));

                break;
            case 4:
                mTitleTextView.setText(R.string.title_final_lower);


                mWinnersOfATourUpperBracket = (ArrayList<String>)mWinnersOfATour.clone();
                mWinnersOfATourLowerBracket.addAll(mLosersOfATour);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATourLowerBracket));
                break;

            case 5:
                mTitleTextView.setText(R.string.title_grand_final);

                mWinnersOfATourUpperBracket.addAll(mWinnersOfATour);
                mPlayersTable.addAll(mLosersOfATour);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATourUpperBracket));
                break;

            case 6:
                showWinner();
                break;
        }

    }

    private void group6Play() {
        if (!mIsGroupStagePlayed) {

            switch (mTourOfAGame) {
                case 1:
                    mTitleTextView.setText(R.string.title_group_stage_first_tour);

                    prepareTourArrays(
                            mTournamentSettings.getGamesConstant(mTourOfAGame),
                            0,
                            0
                    );

                    if (mTournamentSettings.isShuffle()) {
                        Collections.shuffle(mPlayers);
                    }

                    mTournamentRecyclerView.setAdapter(new TournamentGamesGroupAdapter(
                            mTournamentSettings.getGamesConstant(mTourOfAGame), mPlayers));


                    break;
                case 2:
                    mTitleTextView.setText(R.string.title_group_stage_second_tour);

                    prepareTourArrays(
                            mTournamentSettings.getGamesConstant(mTourOfAGame),
                            0,
                            0
                    );

                    mTournamentRecyclerView.setAdapter(new TournamentGamesGroupAdapter(
                            mTournamentSettings.getGamesConstant(mTourOfAGame), mPlayers));

                    break;
                case 3:
                    mTitleTextView.setText(R.string.title_group_stage_third_tour);

                    prepareTourArrays(
                            mTournamentSettings.getGamesConstant(mTourOfAGame),
                            0,
                            0
                    );

                    mTournamentRecyclerView.setAdapter(new TournamentGamesGroupAdapter(
                            mTournamentSettings.getGamesConstant(mTourOfAGame), mPlayers));

                    break;
                case 4:
                    mIsGroupStagePlayed = true;
                    break;
            }

        }


        if (!mIsGroupEndingSet && mIsGroupStagePlayed && !mIsRematchAnalyze) {

            ArrayList<String> firstGroup = new ArrayList<>(mPlayers.subList(0, 3));
            ArrayList<String> secondGroup = new ArrayList<>(mPlayers.subList(3, 6));


            ArrayList<Integer> playersScore = new ArrayList<>(6);

            for (int i = 0, score = 0; i < 6; i++, score = 0) {
                score += mPlayersGroupScoreFirstGame[i];
                score += mPlayersGroupScoreSecondGame[i];

                playersScore.add(0);
                playersScore.set(i, score);
            }

            ArrayList<Integer> playersScoreFirstGroup = new ArrayList<>(playersScore.subList(0, 3));
            ArrayList<Integer> playersScoreSecondGroup = new ArrayList<>(playersScore.subList(3, 6));

            int[] rematchPlayersIndexFirstGroup = rematchDeciderMaxScore(toIntArray(playersScoreFirstGroup));
            int[] rematchPlayersIndexSecondGroup = rematchDeciderMaxScore(toIntArray(playersScoreSecondGroup));

            if (rematchPlayersIndexFirstGroup == null && rematchPlayersIndexSecondGroup == null) {

                for(int i = 0, indexOfMaxFirstGroup = 0, indexOfMaxSecondGroup = 0; i < 5; i+=2) {
                    indexOfMaxFirstGroup = indexOfMax(playersScoreFirstGroup);
                    indexOfMaxSecondGroup = indexOfMax(playersScoreSecondGroup);

                    mPlayers.set(i, firstGroup.remove(indexOfMaxFirstGroup));
                    mPlayers.set(i+1, secondGroup.remove(indexOfMaxSecondGroup));

                    playersScoreFirstGroup.remove(indexOfMaxFirstGroup);
                    playersScoreSecondGroup.remove(indexOfMaxSecondGroup);
                }

                mIsGroupEndingSet = true;
                mTourOfAGame = 1;

                chooseTypeAfterGroup();
                return;

            } else {

                mTitleTextView.setText(R.string.title_group_rematch);

                boolean isRematchFirstGroup = false;
                boolean isRematchSecondGroup = false;

                ArrayList<String> rematchPlayers = new ArrayList<String>();

                if(rematchPlayersIndexFirstGroup != null) {
                    if(rematchPlayersIndexFirstGroup[0] == -1) {
                        mIsTripleRematchFirstGroup = true;

                    } else {
                        isRematchFirstGroup = true;

                        rematchPlayers.add(firstGroup.get(rematchPlayersIndexFirstGroup[0]));
                        rematchPlayers.add(firstGroup.get(rematchPlayersIndexFirstGroup[1]));
                        firstGroup.remove(rematchPlayersIndexFirstGroup[1]);
                        firstGroup.remove(rematchPlayersIndexFirstGroup[0]);
                        mNumberOfRematchGames++;
                    }
                }

                if(rematchPlayersIndexSecondGroup != null) {
                    if(rematchPlayersIndexSecondGroup[0] == -1) {
                        mIsTripleRematchSecondGroup = true;

                    } else {
                        isRematchSecondGroup = true;

                        rematchPlayers.add(secondGroup.get(rematchPlayersIndexSecondGroup[0]));
                        rematchPlayers.add(secondGroup.get(rematchPlayersIndexSecondGroup[1]));
                        secondGroup.remove(rematchPlayersIndexSecondGroup[1]);
                        secondGroup.remove(rematchPlayersIndexSecondGroup[0]);
                        mNumberOfRematchGames++;
                    }
                }

                if(mIsTripleRematchFirstGroup || mIsTripleRematchSecondGroup) {
                    if(mIsTripleRematchFirstGroup && !mIsTripleRematchSecondGroup) {

                        mPlayers.set(0, firstGroup.get(0));
                        mPlayers.set(1, firstGroup.get(1));
                        mPlayers.set(2, firstGroup.get(2));
                    }

                    if(!mIsTripleRematchFirstGroup && mIsTripleRematchSecondGroup) {

                        mPlayers.set(3, secondGroup.get(0));
                        mPlayers.set(4, secondGroup.get(1));
                        mPlayers.set(5, secondGroup.get(2));
                    }

                    if(mIsTripleRematchFirstGroup && mIsTripleRematchSecondGroup) {

                        mPlayers.set(0, firstGroup.get(0));
                        mPlayers.set(1, secondGroup.get(0));

                        mPlayers.set(3, firstGroup.get(2));
                        mPlayers.set(2, secondGroup.get(1));
                        mPlayers.set(4, firstGroup.get(1));
                        mPlayers.set(5, secondGroup.get(2));


                        clearTourArrays();

                        mIsRematchAnalyze = true;
                        mIsGroupEndingSet = true;
                        mTourOfAGame = 1;

                        chooseTypeAfterGroup();
                        return;
                    }
                }

                if(isRematchFirstGroup || isRematchSecondGroup) {
                    prepareTourArrays(
                            mNumberOfRematchGames,
                            mNumberOfRematchGames,
                            mNumberOfRematchGames
                    );

                    if(isRematchFirstGroup) {
                        mPlayers.set(2, firstGroup.get(0));
                    }
                    if(isRematchSecondGroup) {
                        mPlayers.set(5, secondGroup.get(0));
                    }

                    mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                            mNumberOfRematchGames, rematchPlayers));
                }

                mIsRematchAnalyze = true;
                return;
            }
        }

        if(mIsRematchAnalyze) {
            if(mNumberOfRematchGames == 2) {

                mPlayers.set(0, mWinnersOfATour.get(0));
                mPlayers.set(1, mWinnersOfATour.get(1));
                mPlayers.set(3, mLosersOfATour.get(0));
                mPlayers.set(4, mLosersOfATour.get(1));

            } else if(mNumberOfRematchGames == 1) {

                ArrayList<String> targetGroup = null;
                int[] playersScore = new int[3];

                if(mPlayers.indexOf(mLosersOfATour.get(0)) > 2 && !mIsTripleRematchFirstGroup) {
                    targetGroup = new ArrayList<>(mPlayers.subList(0, 3));

                    for (int i = 0; i < playersScore.length; i++) {
                        playersScore[i] = 0;
                        playersScore[i] += mPlayersGroupScoreFirstGame[i];
                        playersScore[i] += mPlayersGroupScoreSecondGame[i];
                    }

                    mPlayers.set(0, targetGroup.remove(indexOfMax(playersScore)));
                    mPlayers.set(1, mWinnersOfATour.get(0));

                    mPlayers.set(3, targetGroup.get(0));
                    mPlayers.set(4, targetGroup.get(1));
                    mPlayers.set(2, mLosersOfATour.get(0));



                } else if (mPlayers.indexOf(mLosersOfATour.get(0)) < 3 && !mIsTripleRematchSecondGroup) {
                    targetGroup = new ArrayList<>(mPlayers.subList(3, 6));

                    for (int i = 3; i < playersScore.length + 3; i++) {
                        playersScore[i - 3] = 0;
                        playersScore[i - 3] += mPlayersGroupScoreFirstGame[i];
                        playersScore[i - 3] += mPlayersGroupScoreSecondGame[i];
                    }


                    mPlayers.set(0, targetGroup.remove(indexOfMax(playersScore)));
                    mPlayers.set(1, mWinnersOfATour.get(0));
                    mPlayers.set(5, mLosersOfATour.get(0));
                    mPlayers.set(3, targetGroup.get(0));
                    mPlayers.set(4, targetGroup.get(1));

                }

            }

            clearTourArrays();

            mIsGroupEndingSet = true;
            mTourOfAGame = 1;
            mIsRematchAnalyze = false;

            chooseTypeAfterGroup();
            return;
        }

        if (mIsGroupEndingSet && mIsGroupStagePlayed) {
            chooseTypeAfterGroup();
        }

    }

    private void groupPlay() {
        if (!mIsGroupStagePlayed) {

            switch (mTourOfAGame) {
                case 1:
                    mTitleTextView.setText(R.string.title_group_stage_first_tour);

                    prepareTourArrays(
                            mTournamentSettings.getGamesConstant(mTourOfAGame),
                            0,
                            0
                    );

                    if (mTournamentSettings.isShuffle()) {
                        Collections.shuffle(mPlayers);
                    }

                    mTournamentRecyclerView.setAdapter(new TournamentGamesGroupAdapter(
                            mTournamentSettings.getGamesConstant(mTourOfAGame), mPlayers));


                    break;
                case 2:
                    mTitleTextView.setText(R.string.title_group_stage_second_tour);

                    prepareTourArrays(
                            mTournamentSettings.getGamesConstant(mTourOfAGame),
                            0,
                            0
                    );

                    mTournamentRecyclerView.setAdapter(new TournamentGamesGroupAdapter(
                            mTournamentSettings.getGamesConstant(mTourOfAGame), mPlayers));

                    break;
                case 3:
                    mTitleTextView.setText(R.string.title_group_stage_third_tour);

                    prepareTourArrays(
                            mTournamentSettings.getGamesConstant(mTourOfAGame),
                            0,
                            0
                    );

                    mTournamentRecyclerView.setAdapter(new TournamentGamesGroupAdapter(
                            mTournamentSettings.getGamesConstant(mTourOfAGame), mPlayers));

                    break;
                case 4:
                    mIsGroupStagePlayed = true;
                    break;
            }

        }

        if (!mIsGroupEndingSet && mIsGroupStagePlayed && !mIsRematchAnalyze) {

            ArrayList<String> firstGroup = new ArrayList<>(mPlayers.subList(0, 3));
            ArrayList<String> secondGroup = new ArrayList<>(mPlayers.subList(3, 6));


            int[] playersScore = new int[6];

            for (int i = 0; i < playersScore.length; i++) {
                playersScore[i] = 0;
                playersScore[i] += mPlayersGroupScoreFirstGame[i];
                playersScore[i] += mPlayersGroupScoreSecondGame[i];
            }

            int[] playersScoreFirstGroup = Arrays.copyOfRange(playersScore, 0, 3);
            int[] playersScoreSecondGroup = Arrays.copyOfRange(playersScore, 3, 6);

            int[] rematchPlayersIndexFirstGroup = rematchDeciderMinScore(playersScoreFirstGroup);
            int[] rematchPlayersIndexSecondGroup = rematchDeciderMinScore(playersScoreSecondGroup);

            if (rematchPlayersIndexFirstGroup == null && rematchPlayersIndexSecondGroup == null) {

                mPlayersTable.add(firstGroup.remove(indexOfMin(playersScoreFirstGroup)));
                mPlayersTable.add(secondGroup.remove(indexOfMin(playersScoreSecondGroup)));

                mPlayers.remove(5);
                mPlayers.remove(4);

                mPlayers.set(0, firstGroup.get(0));
                mPlayers.set(1, secondGroup.get(1));
                mPlayers.set(2, firstGroup.get(1));
                mPlayers.set(3, secondGroup.get(0));

                mIsGroupEndingSet = true;
                mTourOfAGame = 1;

                chooseTypeAfterGroup();
                return;

            } else {

                mTitleTextView.setText(R.string.title_group_rematch);

                boolean isRematchFirstGroup = false;
                boolean isRematchSecondGroup = false;

                ArrayList<String> rematchPlayers = new ArrayList<String>();

                if(rematchPlayersIndexFirstGroup != null) {
                    if(rematchPlayersIndexFirstGroup[0] == -1) {
                        mIsTripleRematchFirstGroup = true;

                    } else {
                        isRematchFirstGroup = true;
                        firstGroup.remove(rematchPlayersIndexFirstGroup[0]);
                        rematchPlayers.addAll(firstGroup);
                        mNumberOfRematchGames++;
                    }
                }

                if(rematchPlayersIndexSecondGroup != null) {
                    if(rematchPlayersIndexSecondGroup[0] == -1) {
                        mIsTripleRematchSecondGroup = true;

                    } else {
                        isRematchSecondGroup = true;
                        secondGroup.remove(rematchPlayersIndexSecondGroup[0]);
                        rematchPlayers.addAll(secondGroup);
                        mNumberOfRematchGames++;
                    }
                }

                if(mIsTripleRematchFirstGroup || mIsTripleRematchSecondGroup) {
                    if(mIsTripleRematchFirstGroup && !mIsTripleRematchSecondGroup) {

                        mPlayersTable.add(firstGroup.remove(new Random(System.currentTimeMillis())
                                .nextInt(3)));

                        mPlayers.set(0, firstGroup.get(0));
                        mPlayers.set(1, firstGroup.get(1));
                        mPlayers.remove(2);
                    }

                    if(!mIsTripleRematchFirstGroup && mIsTripleRematchSecondGroup) {
                        mPlayersTable.add(secondGroup.remove(new Random(System.currentTimeMillis())
                                .nextInt(3)));

                        mPlayers.set(3, secondGroup.get(0));
                        mPlayers.set(4, secondGroup.get(1));
                        mPlayers.remove(5);
                    }

                    if(mIsTripleRematchFirstGroup && mIsTripleRematchSecondGroup) {
                        mPlayersTable.add(firstGroup.remove(new Random(System.currentTimeMillis())
                                .nextInt(3)));
                        mPlayersTable.add(secondGroup.remove(new Random(System.currentTimeMillis())
                                .nextInt(3)));

                        mPlayers.set(0, firstGroup.get(0));
                        mPlayers.set(3, firstGroup.get(1));
                        mPlayers.set(2, secondGroup.get(0));
                        mPlayers.set(1, secondGroup.get(1));


                        clearTourArrays();

                        mIsRematchAnalyze = true;
                        mIsGroupEndingSet = true;
                        mTourOfAGame = 1;

                        chooseTypeAfterGroup();
                        return;
                    }
                }

                if(isRematchFirstGroup || isRematchSecondGroup) {
                    prepareTourArrays(
                            mNumberOfRematchGames,
                            mNumberOfRematchGames,
                            mNumberOfRematchGames
                    );

                    mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                            mNumberOfRematchGames, rematchPlayers));
                }

                mIsRematchAnalyze = true;
                return;
            }
        }

        if(mIsRematchAnalyze) {
            if(mNumberOfRematchGames == 2) {
                mPlayers.remove(mLosersOfATour.get(0));
                mPlayers.remove(mLosersOfATour.get(1));

                mPlayersTable.add(mLosersOfATour.get(0));
                mPlayersTable.add(mLosersOfATour.get(1));

                Collections.swap(mPlayers, 1, 3);

            } else if(mNumberOfRematchGames == 1) {

                ArrayList<String> targetGroup = null;
                int[] playersScore = new int[3];


                if(mPlayers.indexOf(mLosersOfATour.get(0)) > 2 && !mIsTripleRematchFirstGroup) {
                    targetGroup = new ArrayList<>(mPlayers.subList(0, 3));

                    for (int i = 0; i < playersScore.length; i++) {
                        playersScore[i] = 0;
                        playersScore[i] += mPlayersGroupScoreFirstGame[i];
                        playersScore[i] += mPlayersGroupScoreSecondGame[i];
                    }
                    mPlayersTable.add(targetGroup.remove(indexOfMin(playersScore)));
                    mPlayers.remove(0);
                    mPlayers.set(0, targetGroup.get(0));
                    mPlayers.set(1, targetGroup.get(1));

                } else if (mPlayers.indexOf(mLosersOfATour.get(0)) < 3 && !mIsTripleRematchSecondGroup) {
                    targetGroup = new ArrayList<>(mPlayers.subList(3, 6));

                    for (int i = 3; i < playersScore.length + 3; i++) {
                        playersScore[i - 3] = 0;
                        playersScore[i - 3] += mPlayersGroupScoreFirstGame[i];
                        playersScore[i - 3] += mPlayersGroupScoreSecondGame[i];
                    }
                    mPlayersTable.add(targetGroup.remove(indexOfMin(playersScore)));
                    mPlayers.remove(3);
                    mPlayers.set(3, targetGroup.get(0));
                    mPlayers.set(4, targetGroup.get(1));
                }

                mPlayers.remove(mLosersOfATour.get(0));
                mPlayersTable.add(mLosersOfATour.get(0));

                Collections.swap(mPlayers, 1, 3);
            }

            clearTourArrays();

            mIsGroupEndingSet = true;
            mIsRematchAnalyze = false;
            mTourOfAGame = 1;

            chooseTypeAfterGroup();
            return;
        }

        if (mIsGroupEndingSet && mIsGroupStagePlayed) {
            chooseTypeAfterGroup();
        }
    }

    private void double6Play() {
        switch (mTourOfAGame) {
            case 1:
                mTitleTextView.setText(R.string.title_quarterfinal_lower);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        new ArrayList<String>(mPlayers.subList(2, 6))));
                break;

            case 2:
                mTitleTextView.setText(R.string.title_final_upper);

                mWinnersOfATourLowerBracket = (ArrayList<String>)mWinnersOfATour.clone();
                mPlayersTable.addAll(mLosersOfATour);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );

                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        new ArrayList<String>(mPlayers.subList(0, 2))));

                break;

            case 3:
                mTitleTextView.setText(R.string.title_semifinal_lower);

                mWinnersOfATourUpperBracket = (ArrayList<String>)mWinnersOfATour.clone();
                mLosersOfATourUpperBracketDoubleSixHelper.addAll(mLosersOfATour);


                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATourLowerBracket));


                break;

            case 4:
                mTitleTextView.setText(R.string.title_final_lower);

                mWinnersOfATourLowerBracket = (ArrayList<String>)mWinnersOfATour.clone();
                mWinnersOfATourLowerBracket.addAll(mLosersOfATourUpperBracketDoubleSixHelper);
                mPlayersTable.addAll(mLosersOfATour);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATourLowerBracket));

                break;

            case 5:
                mTitleTextView.setText(R.string.title_grand_final);

                mWinnersOfATourUpperBracket.addAll(mWinnersOfATour);
                mPlayersTable.addAll(mLosersOfATour);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATourUpperBracket));

                break;

            case 6:
                showWinner();
                break;
        }


    }

    private void single8Play() {
        switch (mTourOfAGame) {
            case 1:
                mTitleTextView.setText(R.string.title_quarterfinal);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );

                if(mTournamentSettings.isShuffle()) {
                    Collections.shuffle(mPlayers);
                }


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mPlayers));
                break;

            case 2:
                mTitleTextView.setText(R.string.title_semifinal);
                mPlayersTable.addAll(mLosersOfATour);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        0,
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );

                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATour));

                break;

            case 3:
                mTitleTextView.setText(R.string.title_final);
                mPlayersTable.addAll(mLosersOfATour);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        0,
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );

                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATour));

                break;

            case 4:
                showWinner();
                break;
        }

    }

    private void double8Play() {
        switch (mTourOfAGame) {
            case 1:
                mTitleTextView.setText(R.string.title_quarterfinal_upper);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );

                if (mTournamentSettings.isShuffle()) {
                    Collections.shuffle(mPlayers);
                }

                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mPlayers));

                break;
            case 2:
                mTitleTextView.setText(R.string.title_first_round_lower);

                mWinnersOfATourUpperBracket = (ArrayList<String>) mWinnersOfATour.clone();

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        0
                );

                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mLosersOfATour));

                break;

            case 3:
                mTitleTextView.setText(R.string.title_semifinal_upper);
                mPlayersTable.add(mLosersOfATour.get(0));
                mPlayersTable.add(mLosersOfATour.get(1));

                mWinnersOfATourLowerBracket = (ArrayList<String>) mWinnersOfATour.clone();

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATourUpperBracket));

                break;
            case 4:
                mTitleTextView.setText(R.string.title_quarterfinal_lower);

                mWinnersOfATourUpperBracket = (ArrayList<String>) mWinnersOfATour.clone();
                mWinnersOfATourLowerBracket.addAll(mLosersOfATour);
                Collections.swap(mWinnersOfATourLowerBracket, 1, 2);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATourLowerBracket));
                break;

            case 5:
                mTitleTextView.setText(R.string.title_semifinal_lower);

                //mWinnersOfATourUpperBracket.addAll(mWinnersOfATour);
                mPlayersTable.addAll(mLosersOfATour);

                ArrayList<String> helper = (ArrayList<String>) mWinnersOfATour.clone();
                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        0,
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );

                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATour));
                break;

            case 6:
                mTitleTextView.setText(R.string.title_final_upper);

                mWinnersOfATourLowerBracket = (ArrayList<String>) mWinnersOfATour.clone();
                mWinnersOfATourLowerBracket.remove(1);
                mPlayersTable.addAll(mLosersOfATour);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATourUpperBracket));
                break;
            case 7:
                mTitleTextView.setText(R.string.title_final_lower);

                mWinnersOfATourUpperBracket = (ArrayList<String>)mWinnersOfATour.clone();
                mWinnersOfATourLowerBracket.addAll(mLosersOfATour);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATourLowerBracket));
                break;
            case 8:
                mTitleTextView.setText(R.string.title_grand_final);

                mWinnersOfATourUpperBracket.addAll(mWinnersOfATour);
                mPlayersTable.addAll(mLosersOfATour);

                prepareTourArrays(
                        mTournamentSettings.getGamesConstant(mTourOfAGame),
                        mTournamentSettings.getWinnersConstant(mTourOfAGame),
                        mTournamentSettings.getLosersConstant(mTourOfAGame)
                );


                mTournamentRecyclerView.setAdapter(new TournamentGamesAdapter(
                        mTournamentSettings.getGamesConstant(mTourOfAGame), mWinnersOfATourUpperBracket));
                break;
            case 9:
                showWinner();
                break;
        }
    }


    private void showWinner() {
        mPlayersTable.addAll(mLosersOfATour);
        mPlayersTable.add(mWinnersOfATour.get(0));
        Collections.reverse(mPlayersTable);

        startActivityForResult(WinnerActivity.getStartIntent(getActivity(), mPlayersTable),REQUEST_CODE_WINNER);
    }

    private void prepareTourArrays(int nOfGames, int nOfWinners, int nOfLosers) {

        if(nOfGames != 0) {
            mIsASingleGamePlayed = new boolean[nOfGames];
            for (int i = 0; i < nOfGames; i++) {
                mIsASingleGamePlayed[i] = false;
            }
        }

        if(nOfWinners != 0) {
            mWinnersOfATour = new ArrayList<>(nOfWinners);
            for (int i = 0; i < nOfWinners; i++) {
                mWinnersOfATour.add("");
            }
        }

        if(nOfLosers!= 0) {
            mLosersOfATour = new ArrayList<>(nOfLosers);
            for (int i = 0; i < nOfLosers; i++) {
                mLosersOfATour.add("");
            }
        }
    }

    private void clearTourArrays() {
        mIsASingleGamePlayed = new boolean[0];
        mWinnersOfATour = new ArrayList<>(0);
        mWinnersOfATourUpperBracket = new ArrayList<>(0);
        mWinnersOfATourLowerBracket = new ArrayList<>(0);
        mLosersOfATourUpperBracketDoubleSixHelper = new ArrayList<>(0);
        mLosersOfATour = new ArrayList<>(0);
        mPlayersTable = new ArrayList<>(0);
        if(mTournamentSettings.getNumberOfPlayersInt() == 6) {
            mPlayersGroupScoreFirstGame = new int[6];
            mPlayersGroupScoreSecondGame = new int[6];
        }
    }

    private int indexOfMin(int[] array) {
        int minScoreAt = 0;
        for (int i = 0; i < array.length; i++) {
            minScoreAt = array[i] < array[minScoreAt] ? i : minScoreAt;
        }
        return minScoreAt;
    }

    private int indexOfMax(int[] array) {
        int maxScoreAt = 0;
        for (int i = 0; i < array.length; i++) {
            maxScoreAt = array[i] > array[maxScoreAt] ? i : maxScoreAt;
        }
        return maxScoreAt;
    }

    private int indexOfMax(ArrayList<Integer> list) {
        int maxScoreAt = 0;
        for(int i = 0; i < list.size(); i++) {
            maxScoreAt = list.get(i) > list.get(maxScoreAt) ? i : maxScoreAt;
        }
        return maxScoreAt;
    }

    private int[] rematchDeciderMinScore(int[] group) {
        int minScoreAt = indexOfMin(group);

        if(group[0] == group[1] && group[1] == group[2]) {
            return new int[] { -1 };
        }


        if(group[0] == group[1]) {
            if(minScoreAt == 2) {
                return null;
            } else {
                return new int[] { 2 };
            }
        }

        if(group[0] == group[2]) {
            if(minScoreAt == 1) {
                return null;
            } else {
                return new int[] { 1 };
            }
        }

        if(group[1] == group[2]) {
            if(minScoreAt == 0) {
                return null;
            } else {
                return new int[] { 0 };
            }
        }


        return null;
    }

    private int[] rematchDeciderMaxScore(int[] group) {
        int maxScoreAt = indexOfMax(group);

        if(group[0] == group[1] && group[1] == group[2]) {
            return new int[] { -1 };
        }

        if(group[0] == group[1]) {
            if(maxScoreAt == 2) {
                return null;
            } else {
                return new int[] { 0, 1 };
            }
        }

        if(group[0] == group[2]) {
            if(maxScoreAt == 1) {
                return null;
            } else {
                return new int[] { 0, 2 };
            }
        }

        if(group[1] == group[2]) {
            if(maxScoreAt == 0) {
                return null;
            } else {
                return new int[] { 1, 2 };
            }
        }

        return null;
    }

    private void chooseTypeAfterGroup() {
        switch(mTournamentSettings.getTypeOfTournament()) {
            case GROUP_6_SINGLE_4:
                mTournamentSettings.setNumberOfPlayers(TournamentSettings.NumberOfPlayers.FOUR);
                mTournamentSettings.setTypeOfTournament(TournamentSettings.TypeOfTournament.SINGLE_4);
                single4Play();
                break;

            case GROUP_6_DOUBLE_4:
                mTournamentSettings.setNumberOfPlayers(TournamentSettings.NumberOfPlayers.FOUR);
                mTournamentSettings.setTypeOfTournament(TournamentSettings.TypeOfTournament.DOUBLE_4);
                double4Play();
                break;

            case GROUP_6_DOUBLE_6:
                mTournamentSettings.setNumberOfPlayers(TournamentSettings.NumberOfPlayers.SIX);
                mTournamentSettings.setTypeOfTournament(TournamentSettings.TypeOfTournament.DOUBLE_6);
                double6Play();
                break;
        }
    }

    private int[] toIntArray(List<Integer> list) {
        int[] ret = new int[list.size()];
        for(int i = 0; i < ret.length; i++)
            ret[i] = list.get(i);
        return ret;
    }
////

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVED_PLAYERS, mPlayers);
        outState.putSerializable(SAVED_WINNERS, mWinnersOfATour);
        outState.putSerializable(SAVED_LOSERS, mLosersOfATour);
        outState.putSerializable(SAVED_WINNERS_UPPER, mWinnersOfATourUpperBracket);
        outState.putSerializable(SAVED_WINNERS_LOWER, mWinnersOfATourLowerBracket);
        outState.putSerializable(SAVED_LOSERS_SIX_HELPER, mLosersOfATourUpperBracketDoubleSixHelper);
        outState.putSerializable(SAVED_GROUP_SCORE_FIRST_GAME, mPlayersGroupScoreFirstGame);
        outState.putSerializable(SAVED_GROUP_SCORE_SECOND_GAME, mPlayersGroupScoreSecondGame);
        outState.putSerializable(SAVED_SINGLE_GAME_PLAYED, mIsASingleGamePlayed);
        outState.putSerializable(SAVED_PLAYERS_TABLE, mPlayersTable);

        outState.putInt(SAVED_TOUR, mTourOfAGame);
        outState.putInt(SAVED_NUMBER_OF_REMATCH, mNumberOfRematchGames);

        outState.putBoolean(SAVED_NAMES_ENTERED, mIsNamesEntered);
        outState.putBoolean(SAVED_GAMES_PLAYED, mIsGamesPlayed);
        outState.putBoolean(SAVED_GROUP_PLAYED, mIsGroupStagePlayed);
        outState.putBoolean(SAVED_GROUP_ENDING, mIsGroupEndingSet);
        outState.putBoolean(SAVED_REMATCH_ANALYZE, mIsRematchAnalyze);
        outState.putBoolean(SAVED_TRIPLE_REMATCH_FIRST_GROUP, mIsTripleRematchFirstGroup);
        outState.putBoolean(SAVED_TRIPLE_REMATCH_SECOND_GROUP, mIsTripleRematchSecondGroup);
    }

    private void loadSavedState(Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            mPlayers = (ArrayList<String>)savedInstanceState.getSerializable(SAVED_PLAYERS);
            mWinnersOfATour = (ArrayList<String>)savedInstanceState.getSerializable(SAVED_WINNERS);
            mLosersOfATour = (ArrayList<String>)savedInstanceState.getSerializable(SAVED_LOSERS);
            mWinnersOfATourUpperBracket = (ArrayList<String>)savedInstanceState.getSerializable(SAVED_WINNERS_UPPER);
            mWinnersOfATourLowerBracket = (ArrayList<String>)savedInstanceState.getSerializable(SAVED_WINNERS_LOWER);
            mLosersOfATourUpperBracketDoubleSixHelper = (ArrayList<String>)savedInstanceState.getSerializable(SAVED_LOSERS_SIX_HELPER);
            mPlayersGroupScoreFirstGame = (int[])savedInstanceState.getSerializable(SAVED_GROUP_SCORE_FIRST_GAME);
            mPlayersGroupScoreSecondGame = (int[])savedInstanceState.getSerializable(SAVED_GROUP_SCORE_SECOND_GAME);
            mIsASingleGamePlayed = (boolean[])savedInstanceState.getSerializable(SAVED_SINGLE_GAME_PLAYED);
            mPlayersTable = (ArrayList<String>)savedInstanceState.getSerializable(SAVED_PLAYERS_TABLE);

            mTourOfAGame = savedInstanceState.getInt(SAVED_TOUR);
            mNumberOfRematchGames = savedInstanceState.getInt(SAVED_NUMBER_OF_REMATCH);

            mIsNamesEntered = savedInstanceState.getBoolean(SAVED_NAMES_ENTERED);
            mIsGamesPlayed = savedInstanceState.getBoolean(SAVED_GAMES_PLAYED);
            mIsGroupStagePlayed = savedInstanceState.getBoolean(SAVED_GROUP_PLAYED);
            mIsGroupEndingSet = savedInstanceState.getBoolean(SAVED_GROUP_ENDING);
            mIsRematchAnalyze = savedInstanceState.getBoolean(SAVED_REMATCH_ANALYZE);
            mIsTripleRematchFirstGroup = savedInstanceState.getBoolean(SAVED_TRIPLE_REMATCH_FIRST_GROUP);
            mIsTripleRematchSecondGroup = savedInstanceState.getBoolean(SAVED_TRIPLE_REMATCH_SECOND_GROUP);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if(requestCode == REQUEST_CODE_WINNER) {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        }

    }


    public static MainTournamentFragment newInstance(TournamentSettings tournamentSettings) {
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_SETTINGS, tournamentSettings);

        MainTournamentFragment fragment = new MainTournamentFragment();
        fragment.setArguments(args);
        return fragment;
    }
}

