package com.overdrive.tournamentassistant;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

public class WinnerActivity extends SingleFragmentActivity {

    private static final String EXTRA_PLAYER_TABLE = "com.overdrive.tournamentassistant.playertable";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.title_activity_winner);
    }

    @Override
    protected Fragment createFragment() {
        return WinnerFragment.newInstance(
                (ArrayList<String>)getIntent().getSerializableExtra(EXTRA_PLAYER_TABLE));
    }

    public static Intent getStartIntent(Context context, ArrayList<String> playerTable) {
        Intent intent = new Intent(context, WinnerActivity.class);
        intent.putExtra(EXTRA_PLAYER_TABLE, playerTable);
        return intent;
    }
}
