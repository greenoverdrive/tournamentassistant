package com.overdrive.tournamentassistant;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class MainTournamentActivity extends SingleFragmentActivity {

    private static final String EXTRA_SETTINGS = "com.overdrive.tournamentassistant.settings";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.title_activity_main);
    }

    @Override
    protected Fragment createFragment() {
        return MainTournamentFragment.newInstance(
                (TournamentSettings)getIntent().getSerializableExtra(EXTRA_SETTINGS));
    }

    public static Intent getStartIntent(Context context, TournamentSettings tournamentSettings) {
        Intent intent = new Intent(context, MainTournamentActivity.class);
        intent.putExtra(EXTRA_SETTINGS, tournamentSettings);
        return intent;
    }
}
