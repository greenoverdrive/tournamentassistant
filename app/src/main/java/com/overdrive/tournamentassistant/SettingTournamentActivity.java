package com.overdrive.tournamentassistant;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by Overdrive on 13.07.2018.
 */

public class SettingTournamentActivity extends SingleFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.title_activity_settings);
    }


    @Override
    protected Fragment createFragment() {
        return new SettingTournamentFragment();
    }


    public static Intent getStartIntent(Context context) {
        return new Intent(context, SettingTournamentActivity.class);
    }
}
