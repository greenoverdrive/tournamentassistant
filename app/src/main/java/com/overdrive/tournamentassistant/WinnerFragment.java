package com.overdrive.tournamentassistant;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Overdrive on 18.07.2018.
 */

public class WinnerFragment extends Fragment {

    private static final String TAG = "WinnerFragment";
    private static final String BUNDLE_PLAYERS = "players_table";

    private TextView mWinnerTextView;
    private RecyclerView mPlayersTableRecyclerView;
    private Button mContinueButton;

    private ArrayList<String> mPlayersTable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);

        mPlayersTable = (ArrayList<String>)getArguments().getSerializable(BUNDLE_PLAYERS);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_winner, container, false);


        mWinnerTextView = (TextView)v.findViewById(R.id.winner_title);
        mWinnerTextView.setText(getString(R.string.title_winner, mPlayersTable.remove(0)));

        mPlayersTableRecyclerView = (RecyclerView)v.findViewById(R.id.winner_recycler_view);
        mPlayersTableRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mPlayersTableRecyclerView.setAdapter(new NamesAdapter(mPlayersTable.size()));

        mContinueButton = (Button)v.findViewById(R.id.button_continue_winner);
        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        });

        return v;

    }

    private class NamesHolder extends RecyclerView.ViewHolder {

        private TextView mNumberTextView;
        private TextView mNameTextView;
        private int mPosition;

        public NamesHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.winner_name_item, parent, false));

            mNumberTextView = (TextView)itemView.findViewById(R.id.winner_item_number);
            mNameTextView = (TextView)itemView.findViewById(R.id.winner_item_name);

        }

        public void bind(int position) {
            mPosition = position;

            mNumberTextView.setText(getResources()
                    .getString(R.string.item_number, mPosition + 2));
            mNameTextView.setText(mPlayersTable.get(mPosition));
        }

    }

    private class NamesAdapter extends RecyclerView.Adapter<NamesHolder> {

        private int mNumberOfPlayers;

        public NamesAdapter(int numberOfPlayers) {
            mNumberOfPlayers = numberOfPlayers;
        }

        @Override
        @NonNull
        public NamesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new NamesHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull NamesHolder holder, int position) {
            holder.bind(position);
        }

        @Override
        public int getItemCount() {
            return mNumberOfPlayers;
        }

    }



    public static WinnerFragment newInstance(ArrayList<String> playersTable) {
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_PLAYERS, playersTable);

        WinnerFragment fragment = new WinnerFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
