package com.overdrive.tournamentassistant;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by Overdrive on 13.07.2018.
 */

public class StartTournamentFragment extends Fragment {

    private Button mContinueTournamentButton;
    private Button mStartButton;
    private Button mHistoryButton;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_start_tournament, container, false);

        mContinueTournamentButton = (Button)v.findViewById(R.id.button_continue_tournament);
        mContinueTournamentButton.setEnabled(false);

        mStartButton = (Button)v.findViewById(R.id.button_start_tournament);
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SettingTournamentActivity.getStartIntent(getActivity()));
            }
        });

        mHistoryButton = (Button)v.findViewById(R.id.button_history);
        mHistoryButton.setEnabled(false);

        return v;
    }
}
